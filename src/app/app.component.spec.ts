import { TestBed, async } from '@angular/core/testing';

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { ViewportComponent } from './viewport/viewport.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { RightFrameComponent } from './right-frame/right-frame.component';
import { LeftFrameComponent } from './left-frame/left-frame.component';
import { CoordinatesComponent } from './inspector/coordinates/coordinates.component';
import { HierarchyComponent } from './inspector/hierarchy/hierarchy.component';
import { MaterialComponent } from './inspector/material/material.component';
import { BottomFrameComponent } from './bottom-frame/bottom-frame.component';
import { LightComponent } from './inspector/light/light.component';

import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { SelectButtonModule } from 'primeng/selectbutton';
import { DropdownModule } from 'primeng/dropdown';
import { MenuModule } from 'primeng/menu';
import { TreeModule } from 'primeng/tree';
import { AccordionModule } from 'primeng/accordion';
import { GrowlModule } from 'primeng/growl';
import { TabViewModule } from 'primeng/tabview';
import { DragDropModule } from 'primeng/dragdrop';
import { ColorPickerModule } from 'primeng/colorpicker';
import { SliderModule } from 'primeng/slider';

import { MessageService } from 'primeng/components/common/messageservice';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        ViewportComponent,
        ToolbarComponent,
        RightFrameComponent,
        HierarchyComponent,
        MaterialComponent,
        LeftFrameComponent,
        BottomFrameComponent,
        CoordinatesComponent,
        LightComponent
      ],
      imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        // Prime
        InputTextModule,
        ButtonModule,
        SelectButtonModule,
        DropdownModule,
        MenuModule,
        TreeModule,
        AccordionModule,
        GrowlModule,
        TabViewModule,
        DragDropModule,
        ColorPickerModule,
        SliderModule
      ],
      providers: [MessageService],
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  // it(`should have as title 'app'`, async(() => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   const app = fixture.debugElement.componentInstance;
  //   expect(app.title).toEqual('app');
  // }));

  // it('should render title in a h1 tag', async(() => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   fixture.detectChanges();
  //   const compiled = fixture.debugElement.nativeElement;
  //   expect(compiled.querySelector('h1').textContent).toContain('Welcome to wombat-editor!');
  // }));

});
