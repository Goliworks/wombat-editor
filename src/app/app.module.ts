import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ViewportComponent } from './viewport/viewport.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { RightFrameComponent } from './right-frame/right-frame.component';
import { LeftFrameComponent } from './left-frame/left-frame.component';
import { CoordinatesComponent } from './inspector/coordinates/coordinates.component';
import { HierarchyComponent } from './inspector/hierarchy/hierarchy.component';
import { MaterialComponent } from './inspector/material/material.component';
import { BottomFrameComponent } from './bottom-frame/bottom-frame.component';
import { LightComponent } from './inspector/light/light.component';

// PrimeNG modules (UI)
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { SelectButtonModule } from 'primeng/selectbutton';
import { DropdownModule } from 'primeng/dropdown';
import { MenuModule } from 'primeng/menu';
import { TreeModule } from 'primeng/tree';
import { AccordionModule } from 'primeng/accordion';
import { GrowlModule } from 'primeng/growl';
import { TabViewModule } from 'primeng/tabview';
import { DragDropModule } from 'primeng/dragdrop';
import { ColorPickerModule } from 'primeng/colorpicker';
import { SliderModule } from 'primeng/slider';

import { MessageService } from 'primeng/components/common/messageservice';
import { InputSliderComponent } from './common/input-slider/input-slider.component';


@NgModule({
  declarations: [
    AppComponent,
    ViewportComponent,
    ToolbarComponent,
    RightFrameComponent,
    HierarchyComponent,
    MaterialComponent,
    LeftFrameComponent,
    BottomFrameComponent,
    CoordinatesComponent,
    LightComponent,
    InputSliderComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    // Prime
    InputTextModule,
    ButtonModule,
    SelectButtonModule,
    DropdownModule,
    MenuModule,
    TreeModule,
    AccordionModule,
    GrowlModule,
    TabViewModule,
    DragDropModule,
    ColorPickerModule,
    SliderModule
  ],
  providers: [MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
