import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TreeNode } from 'primeng/api';
import { SceneService } from '../services/scene.service';
import { ToolboxService } from '../services/toolbox.service';
import { DragAndDropService } from '../services/drag-and-drop.service';

@Component({
  selector: 'bottom-frame',
  templateUrl: './bottom-frame.component.html',
  styleUrls: ['./bottom-frame.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BottomFrameComponent implements OnInit {

  selectedFolder: TreeNode;
  projectTree: TreeNode[];

  private rootFolder: TreeNode = {
    label: " / ",
    data: "root",
    expandedIcon: "fa fa-folder-open",
    collapsedIcon: "fa fa-folder"
  }

  constructor(public scene: SceneService,
    public dragAndDrop: DragAndDropService,
    private toolbox: ToolboxService) {

    this.projectTree = [this.rootFolder];
    this.selectedFolder = this.rootFolder;

    console.log(scene.environment.assets)

  }

  dropAsset(evt) {
    this.dragAndDrop.draggedObject();
  }

  dragEnter(){
    this.dragAndDrop.dropTarget = 'assets';
  }

  assetFocus(asset){
    this.toolbox.focusAsset.next(asset);
  }

  ngOnInit() {
  }

}
