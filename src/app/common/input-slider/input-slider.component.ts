import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';

@Component({
  selector: 'input-slider',
  templateUrl: './input-slider.component.html',
  styleUrls: ['./input-slider.component.scss']
})
export class InputSliderComponent implements OnInit, OnChanges {


  sourceValue: number;

  @Output() valueChange = new EventEmitter();

  @Input()
  get value(): number{
    return this.sourceValue;
  }
  set value(v:number){
    this.sourceValue = v;
    this.valueChange.emit(this.sourceValue);
  }

  sliderValue: number;
  inputValue: number;

  constructor() { }

  changeInputValue() {
    this.inputValue = this.sliderToInput(this.sliderValue);
    this.sourceValue = this.inputValue;
    this.valueChange.emit(this.sourceValue);
  }

  changeSliderValue() {
    this.sourceValue = this.inputValue;
    this.sliderValue  = this.inputToSlider(this.inputValue);
    this.valueChange.emit(this.sourceValue);
  }

  private inputToSlider(i: number): number {
    return (i * 100) / 2;
  }

  private sliderToInput(i: number): number {
    return (i * 2) / 100;
  }

  ngOnChanges(){
    this.sliderValue = this.inputToSlider(this.sourceValue);
    this.changeInputValue();
  }

  ngOnInit() {
  }

}
