import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoordinatesComponent } from './coordinates.component';
import { Scene, Asset, CubeMesh } from 'wombat-engine';

describe('CoordinatesComponent', () => {
  let component: CoordinatesComponent;
  let fixture: ComponentFixture<CoordinatesComponent>;

  // Sandbox
  let scene = new Scene();
  let cube = new CubeMesh(1, 1, 1);
  scene.add(cube);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CoordinatesComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoordinatesComponent);
    component = fixture.componentInstance;
    component.asset = new Asset('cube', cube, 'Mesh');
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
