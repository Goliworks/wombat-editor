import { Component, OnInit, Input } from '@angular/core';
import { Asset } from 'wombat-engine';

@Component({
  selector: 'coordinates',
  templateUrl: './coordinates.component.html',
  styleUrls: ['./coordinates.component.scss']
})
export class CoordinatesComponent implements OnInit {

  @Input() asset:Asset;

  constructor() { }

  ngOnInit() {
  }

}
