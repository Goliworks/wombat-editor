import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { SceneService } from '../../services/scene.service';
import { DragAndDropService } from '../../services/drag-and-drop.service';
import { ToolboxService } from '../../services/toolbox.service';
import { Asset } from 'wombat-engine';

@Component({
  selector: 'hierarchy',
  templateUrl: './hierarchy.component.html',
  styleUrls: ['./hierarchy.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HierarchyComponent implements OnInit {

  constructor(public scene: SceneService,
    private dragAndDrop: DragAndDropService,
    private toolbox: ToolboxService) { }

  nodeSelect(e) {
    console.log(e);
    console.log(this.scene.selectedElement);
    this.scene.focusObject.next(this.scene.selectedElement.data);
  }

  drop(){
    this.dragAndDrop.draggedObject();
  }

  ngOnInit() {

  } // ngOnInit();

}
