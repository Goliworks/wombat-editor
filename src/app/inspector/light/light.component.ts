import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { Asset } from 'wombat-engine';

@Component({
  selector: 'light',
  templateUrl: './light.component.html',
  styleUrls: ['./light.component.scss']
})
export class LightComponent implements OnInit, OnChanges {

  lightColor: string;
  intensityValue: number;
  intensityPercent: number;

  @Input() asset:Asset;

  constructor() { }

  changeColor(){
    this.asset.data.data.color.setHex(this.strToHex(this.lightColor));
  }

  ngOnChanges(){
    this.lightColor = '#' + this.asset.data.data.color.getHex().toString(16);
  }

  ngOnInit() {
  }

  private strToHex(v: string): number {
    let col = v.replace('#', '0x');
    return Number(col);
  }

}
