import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { SelectItem } from 'primeng/api';
import { SceneService } from '../../services/scene.service';

import { MessageService } from 'primeng/components/common/messageservice';

import { THREE, Asset } from 'wombat-engine';

@Component({
  selector: 'material',
  templateUrl: './material.component.html',
  styleUrls: ['./material.component.scss']
})
export class MaterialComponent implements OnInit, OnChanges {

  materials: SelectItem[];
  selectedMaterial: string;

  diffuseColor: string;

  @Input() asset:Asset;

  constructor(public scene: SceneService, public messageService: MessageService) {

    this.materials = [
      { label: 'Basic', value: 'MeshBasicMaterial' },
      { label: 'Lambert', value: 'MeshLambertMaterial' },
      { label: 'Phong', value: 'MeshPhongMaterial' },
      { label: 'Physical', value: 'MeshPhysicalMaterial' }
    ];

  }

  changeMaterial() {
    
    let material: any;

    switch (this.selectedMaterial) {
      case 'MeshBasicMaterial':
        material = new THREE.MeshBasicMaterial({ color: 0x0000ff });
        break;
      case 'MeshLambertMaterial':
        material = new THREE.MeshLambertMaterial({ color: 0xff0000 });
        break;
      case 'MeshPhongMaterial':
        material = new THREE.MeshPhongMaterial({ color: 0xfff000 });
        break;
      case 'MeshPhysicalMaterial':
        material = new THREE.MeshPhysicalMaterial({ color: 0x00ff00 });
        break;
    }

    // this.messageService.add({ severity: 'warn', summary: 'Warning', detail: 'Function disabled for moment...' });

    material.color = this.asset.data.color;
    material.name = this.asset.data.name; // Temporary : Should be changed in via the engine.

    this.scene.environment.getAsset(this.asset.name, 'material').data = material;
    this.asset.data = material;

    for( let key in this.asset.references){
      this.asset.references[key].material = material;
    }

  };

  changeEmissive() {
    this.asset.data.color.setHex(this.strToHex(this.diffuseColor));
  }

  ngOnChanges(){
    this.selectedMaterial = this.asset.data.type;
    this.diffuseColor = '#' + this.asset.data.color.getHex().toString(16);
  }

  ngOnInit() {}

  private strToHex(v: string): number {
    let col = v.replace('#', '0x');
    return Number(col);
  }

}
