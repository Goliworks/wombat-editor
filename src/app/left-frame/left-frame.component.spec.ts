import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TabViewModule } from 'primeng/tabview';
import { MessageService } from 'primeng/components/common/messageservice';

import { LeftFrameComponent } from './left-frame.component';

describe('LeftFrameComponent', () => {
  let component: LeftFrameComponent;
  let fixture: ComponentFixture<LeftFrameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeftFrameComponent ],
      imports: [TabViewModule],
      providers: [MessageService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeftFrameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
