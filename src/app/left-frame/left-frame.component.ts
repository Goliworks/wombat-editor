import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { SceneService } from '../services/scene.service';
import { ToolboxService } from '../services/toolbox.service';
import { DragAndDropService } from '../services/drag-and-drop.service';

@Component({
  selector: 'left-frame',
  templateUrl: './left-frame.component.html',
  styleUrls: ['./left-frame.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LeftFrameComponent implements OnInit {

  primitives: MenuItem[];
  lights: MenuItem[];
  utils: MenuItem[];

  constructor(public scene: SceneService,
    public dragAndDrop: DragAndDropService,
    private toolbox: ToolboxService) {

    this.primitives = [
      { label: 'Cube', icon: 'cube', command: this.toolbox.createPrimitive.cube },
      { label: 'Sphere', icon: 'sphere', command: this.toolbox.createPrimitive.sphere },
      { label: 'Cylinder', icon: 'cylinder', command: this.toolbox.createPrimitive.cylinder }
    ];

    this.lights = [
      { label: 'Point', icon: 'cube', command: this.toolbox.createLight.point },
      { label: 'Directional', icon: 'cube', command: this.toolbox.createLight.directionnal }
    ];

    this.utils = [
      { label: 'Material', icon: 'sphere', command: this.toolbox.createMaterial }
    ];
  }

  dragAsset(e:Event, fun:Function){
    this.dragAndDrop.draggedObject = fun;
  }

  ngOnInit() {
    // Init at least a cube (temporary).
    // this.toolbox.createPrimitive.cube();
  }

}