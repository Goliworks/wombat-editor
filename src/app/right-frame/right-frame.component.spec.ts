import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { RightFrameComponent } from './right-frame.component';
import { HierarchyComponent } from '../inspector/hierarchy/hierarchy.component';
import { CoordinatesComponent } from '../inspector/coordinates/coordinates.component';
import { MaterialComponent } from '../inspector/material/material.component';
import { LightComponent } from '../inspector/light/light.component';

import { AccordionModule } from 'primeng/accordion';
import { DropdownModule } from 'primeng/dropdown';
import { TreeModule } from 'primeng/tree';
import { ColorPickerModule } from 'primeng/colorpicker';

import { MessageService } from 'primeng/components/common/messageservice';

import { FormsModule } from '@angular/forms';

import { Scene, CubeMesh, Asset } from 'wombat-engine';

describe('RightFrameComponent', () => {
  let component: RightFrameComponent;
  let fixture: ComponentFixture<RightFrameComponent>;

  // Sandbox

  // let scene = new Scene();
  // let cube = new CubeMesh(1, 1, 1);
  // scene.add(cube);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RightFrameComponent,
        HierarchyComponent, CoordinatesComponent, MaterialComponent, LightComponent],
      imports: [FormsModule, AccordionModule,DropdownModule, TreeModule, ColorPickerModule, BrowserAnimationsModule],
      providers:[MessageService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RightFrameComponent);
    component = fixture.componentInstance;
    // component.currentAsset = new Asset('Cube', cube, 'Mesh');
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
