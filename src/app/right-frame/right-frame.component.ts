import { Component, OnInit } from '@angular/core';
import { SceneService } from '../services/scene.service';
import { ToolboxService } from '../services/toolbox.service';
import {SelectItem} from 'primeng/api';

@Component({
  selector: 'right-frame',
  templateUrl: './right-frame.component.html',
  styleUrls: ['./right-frame.component.scss']
})
export class RightFrameComponent implements OnInit {

  currentAsset:any;
  currentMaterial:string;
  selectedMaterial:string;

  constructor(public toolbox:ToolboxService, public scene:SceneService) { 
  }

  changeMaterial(){
    
    try{
      delete this.scene.environment.getAsset(this.currentMaterial, 'material')
        .references[this.currentAsset.data.uuid];
    }
    catch(e){
      
    }

    this.currentAsset.data.data.material = this.scene.environment.getMaterial(this.selectedMaterial);
    this.scene.environment.getAsset(this.selectedMaterial, 'material')
      .references[this.currentAsset.data.uuid] = this.currentAsset.data.data

  }

  ngOnInit() {

    this.toolbox.focusAsset.subscribe((next: any) => {
      if (next) {
        console.log(next);
        this.currentAsset = next;

        if(this.currentAsset.data.category === 'Mesh'){
          this.selectedMaterial = this.currentAsset.data.data.material.name;
          this.currentMaterial = this.selectedMaterial;
        }

      }
    });

  }

}
