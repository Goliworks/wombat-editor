import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DragAndDropService {

  draggedObject:Function;

  dropTarget:String;

  constructor() { }

  dragEnd(){
    this.draggedObject = undefined;
    this.dropTarget = undefined;
    console.info('drag End');
  }

}
