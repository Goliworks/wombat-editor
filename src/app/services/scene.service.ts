import { Injectable } from '@angular/core';
import { Scene, Environment, THREE } from 'wombat-engine';
import { BehaviorSubject } from 'rxjs';

import { TreeNode } from 'primeng/api';

@Injectable({
  providedIn: 'root'
})
export class SceneService {

  environment: Environment;
  scene: Scene;
  tree: TreeNode[];
  selectedElement: TreeNode;

  tcSpace: BehaviorSubject<transformControlSpace>
    = new BehaviorSubject<transformControlSpace>(transformControlSpace.local);

  tcMode: BehaviorSubject<transformControlMode>
    = new BehaviorSubject<transformControlMode>(transformControlMode.translate);

  focusObject: BehaviorSubject<any> = new BehaviorSubject<any>(undefined);

  defaultMaterial = new THREE.MeshLambertMaterial({ color: 0xaaaaaa });

  constructor() {
    this.environment = new Environment();
    this.scene = new Scene();
    this.tree = [];
  }

  addObject(ge: any) {

    // get data from GameEntity
    let obj = ge.data;

    let name = ge.name;
    let i = 0;
    let n = 0;

    while (i < this.tree.length) {
      if (this.tree[i]['label'] == name) {
        n++;
        name = ge.name + '_' + n;
      }
      i++;
    }

    if (n) {
      ge.name = name;
    }

    if('material' in obj){
      obj.material = this.defaultMaterial;
    }

    if(obj.type == 'PointLight'){
      var pointLightHelper = new THREE.PointLightHelper( obj, 0.5 );
      pointLightHelper.name = ge.name;
      this.scene.add(pointLightHelper);
    }
    else if(obj.type == 'DirectionalLight'){
      var directionalLightHelper = new THREE.DirectionalLightHelper( obj, 0.5 );
      directionalLightHelper.name = ge.name;
      this.scene.add(directionalLightHelper);
      // Add target from directional light to scene
      this.scene.add(obj.target);
    }

    let treeData = {
      "label": ge.name,
      "data": ge,
      "expandedIcon": "far fa-image",
      "collapsedIcon": "fas fa-image",
    }
    this.tree.push(treeData);

    this.scene.add(ge);
    this.focusObject.next(ge);

    this.selectedElement = treeData;

  }

}

export enum transformControlSpace {
  local = 'local',
  global = 'world'
}

export enum transformControlMode {
  translate = 'translate',
  rotate = 'rotate',
  scale = 'scale'
}