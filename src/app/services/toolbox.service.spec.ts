import { TestBed, inject } from '@angular/core/testing';
import { MessageService } from 'primeng/components/common/messageservice';

import { ToolboxService } from './toolbox.service';

describe('ToolboxService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ToolboxService, MessageService]
    });
  });

  it('should be created', inject([ToolboxService], (service: ToolboxService) => {
    expect(service).toBeTruthy();
  }));
});
