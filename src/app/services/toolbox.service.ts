import { Injectable } from '@angular/core';
import { THREE, CubeMesh, SphereMesh, CylinderMesh, Asset, GameEntity } from 'wombat-engine';
import { SceneService } from './scene.service';
import { DragAndDropService } from './drag-and-drop.service';
import { BehaviorSubject } from 'rxjs';

import { SelectItem } from 'primeng/api';
import { MessageService } from 'primeng/components/common/messageservice';


@Injectable({
  providedIn: 'root'
})
export class ToolboxService {

  createPrimitive = new CreatePrimitive(this.scene, this.dragAndDrop, this, this.message);
  createLight = new CreateLight(this.scene);

  focusAsset: BehaviorSubject<any> = new BehaviorSubject<any>(undefined);

  materialsList: SelectItem[];

  constructor(private scene: SceneService, private dragAndDrop: DragAndDropService, private message: MessageService) {}

  createMaterial = (name: string = 'Material') => {
    this.scene.environment.addMaterial(name, new THREE.MeshLambertMaterial({ color: 0xaaaaaa }));
    this.refreshMaterialsList();
  }

  private refreshMaterialsList() {
    this.materialsList = [];
    let list = this.scene.environment.assets.filter((e) => {
      return e.type === 'material';
    })
    console.log(list);

    for (let i in list) {
      console.log(list[i].name);
      this.materialsList.push({ label: list[i].name, value: list[i].name });
    }

  }
}

class CreateLight {

  private light: any;
  private name: string;
  private gameEntity: GameEntity;

  constructor(private scene: SceneService){ }

  point = () => {
    this.light = new THREE.PointLight(0xffffff, 1, 100);
    this.light.position.set(0, 0, 0);
    this.name = 'Light';
    this.create();
  }

  directionnal = () => {
    // Not optimal...
    this.light = new THREE.DirectionalLight(0xffffff, 1);
    this.light.position.set(4, 4, 4);
    this.light.target.position.set( 0, 0, 0 );
    this.name = 'Light';
    this.create();
  }

  private create(){
    this.light.name = this.name;
    this.gameEntity = new GameEntity(this.light);
    this.gameEntity.category = 'Light';
    this.scene.addObject(this.gameEntity);
  }

}

class CreatePrimitive {

  private mesh: any;
  private name: string;
  private gameEntity: GameEntity;

  constructor(private scene: SceneService,
    private dragAndDrop: DragAndDropService,
    private toolbox: ToolboxService,
    private message: MessageService) { }

  cube = () => {
    this.mesh = new CubeMesh(1, 1, 1);
    this.name = 'Cube';
    this.create();
  }

  sphere = () => {
    this.mesh = new SphereMesh(0.8, 15, 15);
    this.name = 'Sphere';
    this.create();
  }

  cylinder = () => {
    this.mesh = new CylinderMesh(0.5, 0.5, 1.5, 15, 1);
    this.name = 'Cylinder';
    this.create();
  }

  private create() {

    this.mesh.name = this.name;

    this.gameEntity = new GameEntity(this.mesh);

    if (this.dragAndDrop.dropTarget == 'assets') {
      // this.scene.environment.addMesh(this.name, this.mesh);
      this.message.add({ severity: 'warn', summary: 'Warning', detail: 'Function disabled for moment...' });
    }
    else {
      this.scene.addObject(this.gameEntity);
      this.toolbox.focusAsset.next(new Asset(this.name, this.gameEntity, 'GameEntity', true));
       
    }

    this.gameEntity = undefined;
    this.mesh = undefined;
    this.name = undefined;
  }

}