import { Component, OnInit } from '@angular/core';
import { SceneService, transformControlMode, transformControlSpace } from '../services/scene.service';

import { SelectItem, MenuItem } from 'primeng/api';

@Component({
  selector: 'toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  transformMode: SelectItem[];
  transformSpace: SelectItem[];

  items: MenuItem[];

  selectedMode: transformControlMode = transformControlMode.translate;
  selectedSpace: transformControlSpace = transformControlSpace.local;


  constructor(private scene: SceneService) {

    this.transformMode = [
      { value: transformControlMode.translate, icon: 'fas fa-arrows-alt' },
      { value: transformControlMode.rotate, icon: 'fas fa-redo-alt' },
      { value: transformControlMode.scale, icon: 'fas fa-external-link-alt' }
    ];

    this.transformSpace = [
      { label: 'Local', value: 'local' },
      { label: 'Global', value: 'world' }
    ];

  }

  ngOnInit() {
    
  }

  changeMode() {
    this.scene.tcMode.next(this.selectedMode);
  }

  changeSpace() {
    this.scene.tcSpace.next(this.selectedSpace);
  }

}