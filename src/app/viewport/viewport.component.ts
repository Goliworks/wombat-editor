import { Component, OnInit, AfterViewInit, ElementRef, ViewChild } from '@angular/core';
import { SceneService, transformControlMode, transformControlSpace } from '../services/scene.service';
import { DragAndDropService } from '../services/drag-and-drop.service';
import { ToolboxService } from '../services/toolbox.service';
import { Camera, Viewport, Asset, THREE } from 'wombat-engine';

@Component({
  selector: 'viewport',
  templateUrl: './viewport.component.html',
  styleUrls: ['./viewport.component.scss']
})
export class ViewportComponent implements OnInit, AfterViewInit {

  @ViewChild('viewport') canvas: ElementRef;

  keys: {[key:string]:boolean} = {
    'Control':false,
    'Alt':false,
  }

  private camera: Camera;
  private viewport: Viewport;

  private grid: THREE.GridHelper;
  private viewControls: THREE.OrbitControls;
  private transformControls: THREE.TransformControls;

  private mouse = new THREE.Vector2;
  private raycaster = new THREE.Raycaster;

  private intersected: any;

  constructor(public scene: SceneService,
    private dragAndDrop: DragAndDropService,
    private toolbox:ToolboxService) {
    this.grid = new THREE.GridHelper(15, 20, 0x344e59, 0x888888);
    this.grid.name = 'Helper';
    this.grid.position.y = 0;
  }

  ngOnInit() {
    addEventListener('keydown', (e) => {
      if(this.keys[e.key] !== undefined){
        this.keys[e.key] = true;
        this.activeControls();
      }
    });
    addEventListener('keyup', (e) => {
      if(this.keys[e.key] !== undefined){
        this.keys[e.key] = false;
        this.activeControls();
      }
    });
    addEventListener('click', (e) => {
      if(!this.keys['Control']){
        this.raycasting(e);
        console.log(this.intersected);
      }
    }, false);
  }

  private init = () => {
    this.transformControls.update();
  }

  private windowResize = () => {
    this.canvas.nativeElement.style.width = "100%";
    this.canvas.nativeElement.style.height = "100%";

    let viewWidth = this.canvas.nativeElement.offsetWidth;
    let viewHeight = this.canvas.nativeElement.offsetHeight;

    this.camera.t_obj.aspect = viewWidth / viewHeight;
    this.camera.t_obj.updateProjectionMatrix();
    this.viewport.renderer.setSize(viewWidth, viewHeight);
  }

  private activeControls(){
    console.log(this.keys);
    if(this.keys['Control']){
      this.viewControls.enablePan = true;
      this.viewControls.enableRotate = true;
      if(this.keys['Alt']){
        this.viewControls.screenSpacePanning = false;
      }
      else{
        this.viewControls.screenSpacePanning = true;
      }
    }
    else{
      this.viewControls.enablePan = false;
      this.viewControls.enableRotate = false;
    }
  }

  private raycasting(e){

    let rect = this.viewport.renderer.domElement.getBoundingClientRect();
    this.mouse.x = ( (e.clientX - rect.left) / this.viewport.renderer.domElement.offsetWidth) * 2 - 1;
    this.mouse.y = - ( (e.clientY - rect.top) / this.viewport.renderer.domElement.offsetHeight) * 2 + 1;
    
    this.raycaster.setFromCamera( this.mouse, this.camera.t_obj );

    // calculate objects intersecting the picking ray
    let intersects = this.raycaster.intersectObjects( this.scene.scene.scene.children );

    if ( intersects.length > 0 ) {
      this.intersected = null;
      for(let i in intersects){
        if(intersects[i].object.name !== 'Helper'){
          this.intersected = intersects[i].object;
          break;
        }
      }
    }
    else{
      this.intersected = null;
    }

    if(this.intersected){
      let data = this.scene.scene.children[this.intersected.name];
      this.scene.focusObject.next(data);
    }
  }

  ngAfterViewInit() {

    this.camera = new Camera(75, this.canvas.nativeElement.offsetWidth / this.canvas.nativeElement.offsetHeight, 0.1, 1000);

    this.viewport = new Viewport(this.scene.scene, this.camera, this.canvas.nativeElement);

    this.viewport.renderer.setClearColor(0x5e5e5e, 1);

    this.camera.position.z = 8;
    this.camera.position.y = 5;

    this.scene.scene.add(this.grid);

    this.viewControls = new THREE.OrbitControls(this.camera.t_obj, this.viewport.renderer.domElement);
    this.viewControls.target.set(0, 0, 0);
    this.viewControls.zoomSpeed = 2;
    this.viewControls.screenSpacePanning = true;
    this.viewControls.enableKeys = false;

    this.activeControls();

    this.transformControls = new THREE.TransformControls(this.camera.t_obj, this.viewport.renderer.domElement);

    this.scene.tcMode.subscribe((next: transformControlMode) => {
      this.transformControls.setMode(next)
    });

    this.scene.tcSpace.subscribe((next: transformControlSpace) => {
      console.log(next);
      this.transformControls.setSpace(next)
    });

    this.scene.focusObject.subscribe((next: any) => {
      if (next) {

        let data:any;

        if(next.type == 'GameEntity'){
          data = next.data;
        }
        else {
          data = next;
        }

        if('obj' in data){
          this.transformControls.attach(data.obj);
        }
        else{
          this.transformControls.attach(data);
        }

        this.toolbox.focusAsset.next(
          new Asset(next.name , next, 'GameEntity', true)
        )
        
      }
    })

    this.scene.scene.add(this.transformControls);
    this.viewport.render(this.init);

    window.addEventListener('resize', this.windowResize);

  }

  drop(e:Event){
    this.dragAndDrop.draggedObject();
  }

}
